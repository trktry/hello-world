package com.xkrm.greeting;

/**
 * Created by kerem on 7.07.2017.
 */

import java.util.concurrent.atomic.AtomicLong;

import com.xkrm.greeting.Greeting;
import org.springframework.beans.factory.support.SecurityContextProvider;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GreetingController {

    private static final String template = "Hello, %s!";
    private final AtomicLong counter = new AtomicLong();

    @RequestMapping("/greeting")
        public Greeting greeting(@RequestParam(value="name", defaultValue="World") String name) {
        return new Greeting(counter.incrementAndGet(),
                String.format(template, name));
    }
}