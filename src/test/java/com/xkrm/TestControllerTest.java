package com.xkrm;

import com.xkrm.greeting.GreetingController;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;

/**
 * Created by kerem on 6.07.2017.
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class TestControllerTest {

    @Autowired
    private TestController testController;

    //TODO : Dependency Injection
    @Test
    public void testControllerShouldReturnHello() throws Exception {
        TestController testController2 = new TestController();
        testController2.index();
        String response = testController.index();
        assertEquals(response,"Hello World");

    }

}
