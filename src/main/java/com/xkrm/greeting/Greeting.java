package com.xkrm.greeting;

import javax.validation.constraints.NotNull;

/**
 * Created by kerem on 4.07.2017.
 */
public class Greeting {

    private final long id;

    @NotNull
    private final String content;

    public Greeting(long id, String content) {
        this.id = id;
        this.content = content;
    }

    public long getId() {
        return id;
    }

    public String getContent() {
        return content;
    }
}
