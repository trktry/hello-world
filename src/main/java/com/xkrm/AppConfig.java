package com.xkrm;

/**
 * Created by kerem on 6.07.2017.
 */
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = {"com.mkyong.examples.spring"})
public class AppConfig {
}