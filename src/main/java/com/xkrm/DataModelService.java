package com.xkrm;

/**
 * Created by kerem on 6.07.2017.
 */
public interface DataModelService {

    boolean isValid(String input);

}
