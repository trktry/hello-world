package com.xkrm;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by kerem on 4.07.2017.
 */
@RestController
public class TestController {

    @GetMapping("/")
    public String index() {
        return "Hello World";


    }
}
